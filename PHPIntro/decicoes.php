<?php

$idade = 17;
$numeroDePessoas = 2;

echo "Você só pode entrar se tiver mais do que 18 anos" . PHP_EOL;

if ($idade >= 18) {
  echo "Você tem $idade anos." . PHP_EOL; 
  echo "Pode entrar";
} else if ($idade >= 16 && $numeroDePessoas >=2) {
    echo "Você tem $idade anos e está acompanhado(a), então pode entrar.";
  } else{
    echo "Você só tem $idade anos. Você não pode entrar.";
  }

echo PHP_EOL;
echo "Adeus!";

$mensagem = $idade <= 18 ? "menor" : "maior";

echo $mensagem;